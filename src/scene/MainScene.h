#pragma once

#include "Scene.h"

class MainScene : public Scene
{
    float time_ = 0;
    bool init_ = false;
    EntityId loadingWindowId_;
    EntityId playerId_;
    bool mouseHandled_ = false;

public:
    void Init() override;

    void Draw() override;

    void Update(const double &dt) override;

    IScene *SwitchScene() override;
};
