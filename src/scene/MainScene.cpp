#include "MainScene.h"

#include "entity/Entities.h"
#include "ComponentStore.h"
#include "component/CDrawable.h"
#include "component/CTransform.h"
#include "component/CDrag.h"
#include "component/CAnimation.h"
#include "component/CText.h"
#include "component/CLoading.h"
#include "component/CFrame.h"
#include "component/CPropulsion.h"
#include "ForcesSystem.h"
#include "ResourceStore.h"

EntityId LoadingWindow();

void MainScene::Init()
{
    loadingWindowId_ = LoadingWindow();
}

void MainScene::Draw()
{
    mouseHandled_ = false;
    time_ += rl::GetFrameTime();
    ComponentStore::ForEach<CDrawable>([](CDrawable* drawable)
    {
        drawable->Draw();
    });
    if(time_ >= 2.f && !init_)
    {
        init_ = true;
        EntityBuilder::Delete(loadingWindowId_);
        playerId_ = Entity::Player();
    }
    /*ComponentStore::ForEach<CDrag>([](CDrag* drag)
        {
            drag->update();
        });
    ComponentStore::ForEach<CAnimation>([](CAnimation* animation)
        {
            animation->update(rl::GetFrameTime());
        });*/

    // If no component handled the mouse event, then the event occurred on the background
    if(rl::IsMouseButtonPressed(rl::MOUSE_RIGHT_BUTTON) && !mouseHandled_)
    {
        auto* cPropulsion = ComponentStore::Get<CPropulsion>(playerId_);
        cPropulsion->target = { static_cast<float>(rl::GetMouseX()), static_cast<float>(rl::GetMouseY()) };
        cPropulsion->StartEngine();
    }
}

void MainScene::Update(const double &dt)
{
    ForcesSystem::Update(dt);
}

IScene *MainScene::SwitchScene()
{
    return nullptr;
}

EntityId LoadingWindow()
{
    EntityId id = Entity::Window();

    auto* text = ComponentStore::Build<CText>(id);
    text->text = "Loading...";
    rl::Vector2 textSize = text->getSize();
    int textWidth = static_cast<int>(textSize.x);
    int textHeight = static_cast<int>(textSize.y);

    auto* l = ComponentStore::Build<CLoading>(id);
    l->size = 80;
    rl::Vector2 lSize = l->getSize();
    int lw = static_cast<int>(lSize.x);
    int lh = static_cast<int>(lSize.y);

    auto* frame = ComponentStore::Get<CFrame>(id);
    auto* transform = ComponentStore::Get<CTransform>(id);
    constexpr int padding = 10;
    text->offsetX = padding;
    text->offsetY = padding;
    l->offsetX = padding;
    l->offsetY = text->offsetY + textHeight + padding;
    frame->width = std::max(textWidth, lw) + padding * 2;
    frame->height = textHeight + lh + padding * 3;
    float x = std::round(rl::GetScreenWidth() / 2.f - frame->width / 2.f);
    float y = std::round(rl::GetScreenHeight() / 2.f - frame->height / 2.f);
    transform->position = { x, y };

    return id;
}
