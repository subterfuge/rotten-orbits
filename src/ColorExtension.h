#pragma once

#include "Color.h"

namespace Color
{
    constexpr rl::Color WindowCorner = White;
    constexpr rl::Color WindowBorder = { White.r, White.g, White.b, 80 };
    constexpr rl::Color Text = White;
}
