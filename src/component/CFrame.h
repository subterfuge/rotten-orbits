#pragma once

#include "raylib_namespace.h"
#include "Alias.h"
#include "Color.h"
#include "ComponentStore.h"
#include "component/CTransform.h"
#include "CDrawable.h"
#include "Debug.h"

class CFrame : public CDrawable
{
public:
    DEFINE_COMPONENT_DERIVED(CDrawable, CFrame)

    int width, height;
    int borderThickness = 1; // For now, the thickness can only be one
    rl::Color borderColor, cornerColor, color;
    int offsetX, offsetY;
    int cornerSize;

    explicit CFrame(const EntityId& entity);

    void Draw() override;

#ifdef DEBUG_TOOLS

    void DebugGui() override;

#endif

};
