#pragma once

#include "raylib_namespace.h"
#include "Component.h"

class CPropulsion : public Component
{
public:
    DEFINE_COMPONENT(CPropulsion)

    explicit CPropulsion(const EntityId& entity);

    float maxSpeed = 60.f; ///< Max propulsion speed in unit (namely pixels) per seconds
    float acceleration = 40.f; ///< Gain in speed per second. Also used for deceleration
    rl::Vector2 target; ///< Target of the object
    rl::Vector2 targetVector;
    rl::Vector2 steeringVector;
    bool breaking = true;
    bool stopped = true;

    /// Time to reach the given speed from 0 using the current acceleration. Note that it also works
    /// to calculate break time, from the given speed to 0, as CPropulsion uses the same variable for
    /// acceleration and deceleration
    /// @return Time to reach speed or break time from speed
    float GetAccelerationTime(float speed) const
    {
        return speed / acceleration;
    }

    float GetBreakDistance(float speed) const
    {
        // We divide by 2 the speed to get mean speed because final speed is 0 - assuming deceleration is constant
        return (speed / 2.f) * (GetAccelerationTime(speed));
        // or
        //return std::pow(speed, 2.f) / ((GetAccelerationTime(speed)) * 2.f);
    }

    void StartEngine();

#ifdef DEBUG_TOOLS
public:
    void DebugGui() override;

    void DebugDraw() override;

private:
    bool displayCheckbox_ = false;
#endif
};

