#include "CDialogText.h"

#include "Debug.h"
#include "ResourceStore.h"
#include "ComponentStore.h"
#include "component/CTransform.h"

CDialogText::CDialogText(const EntityId &entity)
    : CDrawable(entity)
{}

void CDialogText::Draw()
{
    UpdateCharsCount();
    auto* t = ComponentStore::Get<CTransform>(GetEntityId());
    std::string drawnText = text.substr(0, drawnCharsCount_);
    rl::Vector2 position = { t->position.x + offset.x, t->position.y + offset.y };
    rl::Font font = TextureAtlas::GetFont();
    rl::DrawTextEx(font, drawnText.c_str(), position, font.baseSize, 0.f, color);
}

void CDialogText::UpdateCharsCount()
{
    if(!playing || drawnCharsCount_ >= text.size())
        return;

    if(text[drawnCharsCount_] == '\0')
        return;

    time_ += rl::GetFrameTime();
    while(time_ >= 1.f / speed)
    {
        rl::Sound bleep = AudioResource::GetBleep(AudioResource::Bleep::Click3);
        rl::SetSoundVolume(bleep, 0.05f);
        if(!rl::IsSoundPlaying(bleep))
            rl::PlaySound(bleep);
        time_ -= 1.f / speed;
        ++drawnCharsCount_;
    }
}

#ifdef DEBUG_TOOLS

void CDialogText::DebugGui()
{
    ImGui::PushID(DEBUG_GUI_ID(CDialogText));
    if(ImGui::CollapsingHeader("Dialog Text"))
    {
        ImGui::PushItemWidth(65.f);
        ImGui::DragFloat("##x", &offset.x, 1.f, 0.f,
                         static_cast<float>(rl::GetScreenWidth()), "x: %.1f");
        ImGui::SameLine();
        ImGui::DragFloat("##y", &offset.y, 1.f, 0.f,
                         static_cast<float>(rl::GetScreenHeight()), "y: %.1f");
        ImGui::SameLine();
        ImGui::Text("Offset");
        ImGui::PopItemWidth();

        ImGui::Text("Text");
        static int capToAdd = 20;
        ImGui::DragInt("##capToAddValue", &capToAdd, 1, 0, 1000);
        ImGui::SameLine();
        if(ImGui::Button("Add capacity to string"))
            text.reserve(text.capacity() + capToAdd);
        char* txtTmp = text.data();
        ImGui::InputTextMultiline("##text", txtTmp, text.capacity(), { 180.f, 80.f });
        text.assign(txtTmp);

        ImGui::DragFloat("Speed", &speed, 0.5f, 0.0f, 1000.f, "%.1f c/s");

        if(ImGui::ColorButton("Color##btn", Color::RlToIm(color)))
            Debug::DisplayColorPicker(&color);
        ImGui::SameLine();
        if(ImGui::SmallButton("Color"))
            Debug::DisplayColorPicker(&color);

        ImGui::SliderInt("Drawn chars", &drawnCharsCount_, 0, text.size());
        if(ImGui::Button("Play"))
            playing = true;
        ImGui::SameLine();
        if(ImGui::Button("Pause"))
            playing = false;
    }
    ImGui::PopID();
}

#endif
