#pragma once

#include "Component.h"
#include "raylib_namespace.h"

class CMass : public Component
{
public:
    DEFINE_COMPONENT(CMass)

    float mass = 1.f;
    rl::Vector2 velocity = { 0.f, 0.f };

    explicit CMass(const EntityId& Entity) : Component(Entity)
    {}

    /// Get speed using velocity length
    /// @return The speed
    float GetSpeed() const
    {
        return rl::Vector2Length(velocity);
    }
};
