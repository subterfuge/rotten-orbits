#include "CTexture.h"

#include "ResourceStore.h"
#include "component/CTransform.h"

CTexture::CTexture(const EntityId& entity)
        : CDrawable(entity)
{}

void CTexture::Draw()
{
    auto* t = ComponentStore::Get<CTransform>(GetEntityId());
    rl::Vector2 pos;
    if(t == nullptr)
        pos = { static_cast<float>(offsetX), static_cast<float>(offsetY) };
    else
        pos = { std::round(t->position.x + offsetX), std::round(t->position.y + offsetY) };
    rl::DrawTextureRec(TextureAtlas::GetTexture(), textureRectangle, pos, color);
}

rl::Vector2 CTexture::GetSize()
{
    return { textureRectangle.width, textureRectangle.height };
}

#ifdef DEBUG_TOOLS

void CTexture::DebugGui()
{

}

void CTexture::DebugDraw()
{

}

#endif
