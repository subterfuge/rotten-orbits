#pragma once

#include "Component.h"
#include "Vector2.h"
#include "Maths.h"

class CDragBack : public Component
{
public:
    DEFINE_COMPONENT(CDragBack);

    int x = 0, y = 0;

    CDragBack(const EntityId& entity) : Component(entity)
    {}

    void computeXFrame(int& xFrame)
    {
        compute(xFrame, x);
    }

    void computeYFrame(int& yFrame)
    {
        compute(yFrame, y);
    }

    void reset()
    {
        x = y = 0;
    }

private:
    void compute(int& value, int& dragCoord)
    {
        if(dragCoord == 0)
            return;

        if(dragCoord > 0)
        {
            if(dragCoord + value < 0)
            {
                value = dragCoord + value;
                dragCoord = 0;
            }
            else
            {
                dragCoord += value;
                value = 0;
            }
        }
        if(dragCoord < 0)
        {
            if(dragCoord + value > 0)
            {
                value = dragCoord + value;
                dragCoord = 0;
            }
            else
            {
                dragCoord += value;
                value = 0;
            }
        }
    }

#ifdef DEBUG_TOOLS

public:
    void DebugGui() override
    {
        if(ImGui::CollapsingHeader(("Drag-Back##" + StringId()).c_str()))
        {
            ImGui::BeginDisabled(true);
            ImGui::SetNextItemWidth(65.f);
            ImGui::DragInt(("##dragBackX" + StringId()).c_str(), &x, 0, 0, 0, "x: %i");
            ImGui::SetNextItemWidth(65.f);
            ImGui::SameLine();
            ImGui::DragInt(("##dragBackY" + StringId()).c_str(), &y, 0, 0, 0, "y: %i");
            ImGui::EndDisabled();
            ImGui::SameLine();
            ImGui::Text("Drag-back position");
        }
    }

#endif
};
