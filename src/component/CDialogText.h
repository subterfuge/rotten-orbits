#pragma once

#include <string>
#include "CDrawable.h"
#include "ColorExtension.h"
#include "raylib_namespace.h"

class CDialogText : public CDrawable
{
public:
    DEFINE_COMPONENT_DERIVED(CDrawable, CDialogText);

    std::string text = "";
    float speed = 30.f; ///< Speed of apparition of chars, in char per second
    float dotPause = 0.5f; ///< Amount of time to pause after a dot char, in seconds
    float commaPause = 0.25f; ///< Amount of time to pause after a comma char, in seconds
    rl::Vector2 offset = { 0.f, 0.f };
    rl::Color color = Color::Text;
    bool playing = false;

private:
    float time_ = 0.f;
    int drawnCharsCount_ = 0;

public:
    explicit CDialogText(const EntityId& entity);

    void Draw() override;

private:
    void UpdateCharsCount();

#ifdef DEBUG_TOOLS

public:
    void DebugGui() override;

#endif
};
