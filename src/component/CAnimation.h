#pragma once

#include <functional>
#include "Component.h"
#include "raylib_namespace.h"
#include "implot.h"

/*enum class Easing
{
    LINEAR,     ///< Linear interpolation, no easing done. Default for CAnimation's keyframes

    SINE_IN,
    SINE_OUT,
    SINE_INOUT,

    CUBIC_IN,
    CUBIC_OUT,
    CUBIC_INOUT,
};*/

namespace Easing
{
    float Linear(float x);

    float SineIn(float x);
    float SineOut(float x);
    float SineInOut(float x);

    float CubicIn(float x);
    float CubicOut(float x);
    float CubicInOut(float x);
}

struct Keyframe
{
    /// Time value in seconds to reach this keyframe from the previous one. If this value equals 0,
    /// this keyframe must be reached the same frame as the previous one and the previous one shouldn't be
    /// played.
    float timestamp = 0.f;

    /// Value usually going from 0 to 1 and used as a factor for the animation
    float value = 0.f;

    /// Easing curve used to define the way the value goes from the previous keyframe to this one
    float (*easingCurve)(float) = &Easing::Linear;
};

class CAnimation : public Component
{
public:
    DEFINE_COMPONENT(CAnimation);

    /// Current animation time, i.e. duration after calling play, without the pauses.
    float time = 0.f;

    bool loop = false;

    /// List of keyframes for the animation. If the first Keyframe::timestamp differs from 0.f, the animation starts
    /// with a fictitious keyframe with a value set to 0.f. Else, the animation starts with the first given
    /// CAnimation::keyframe and its Keyframe::easingCurve is ignored
    std::vector<Keyframe> keyframes;

private:
    bool isPaused = true;
    bool stopNextFrame = false;
    int currentKeyframeId = 0;
    float currentKeyframeTime = 0.f;
    std::function<void(CAnimation*)> onBeginFunction;
    std::function<void(CAnimation*)> onEndFunction;
    std::function<void(EntityId, float)> onPlayFunction;

public:
    explicit CAnimation(const EntityId& entity)
            : Component(entity), keyframes()
    {}

    virtual ~CAnimation() override;

    /// Play the animation from where it paused, or from the start if it's not in pause
    void play();

    /// Pause the animation
    void pause();

    /// Reset CAnimation::time, call onPlay's function one last time and pause the animation.
    /// CAnimation::onEnd 's function is not called when stopped this way
    void stop();

    void addKeyframe(const Keyframe& kf);

    /// Define the function played just before the animation is played from the beginning
    void onBegin(const std::function<void(CAnimation*)>& function);

    void onEnd(const std::function<void(CAnimation*)>& function);

    void onPlay(const std::function<void(EntityId, float)>& function);

    void update(float dt);

    /// Get animation duration in seconds
    float getDuration();

private:
    void updateCurrentKeyframe();

    float getCurrentKeyframeValue();

    float getKeyframeValue(float atTime);

#if (DEBUG_LEVEL >= 2)

private:
    float* curveXs;
    float* curveYs;
    bool isCurveInit = false;
    bool curveNeedsUpdate = true;

    /// Getter callback for curve points
    //ImPlotPoint pointGetter(int idx, void* userData);

public:
    void DebugGui() override;

    void computeCurve();

#endif
};
