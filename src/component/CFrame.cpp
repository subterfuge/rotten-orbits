#include "CFrame.h"

#include "imgui.h"
#include "ResourceStore.h"

CFrame::CFrame(const EntityId &entity)
        : CDrawable(entity), width(0), height(0), borderThickness(0), cornerSize(5),
          borderColor(rl::BLACK), color(rl::BLACK), cornerColor(rl::WHITE), offsetX(0), offsetY(0)
{}

void CFrame::Draw() {
    auto *t = ComponentStore::Get<CTransform>(GetEntityId());
    if (t == nullptr)
        return;

    float lx = t->position.x + static_cast<float>(offsetX);
    float ly = t->position.y + static_cast<float>(offsetY);
    rl::DrawRectangle(lx, ly, width, height, color);
    rl::DrawRectangleLines(lx, ly, width, height, borderColor);

    float maxLx = lx + width;
    float maxLy = ly + height;
    rl::DrawTextureRec(TextureAtlas::GetTexture(), TextureAtlas::TopLeftCorner, { lx, ly }, cornerColor);
    rl::DrawTextureRec(TextureAtlas::GetTexture(), TextureAtlas::BottomLeftCorner, { lx, maxLy - 5.f }, cornerColor);
    rl::DrawTextureRec(TextureAtlas::GetTexture(), TextureAtlas::TopRightCorner, { maxLx - 5.f, ly }, cornerColor);
    rl::DrawTextureRec(TextureAtlas::GetTexture(), TextureAtlas::BottomRightCorner, { maxLx - 5.f, maxLy - 5.f }, cornerColor);
}

#ifdef DEBUG_TOOLS

void CFrame::DebugGui()
{
    if(ImGui::CollapsingHeader(("Frame##" + StringId()).c_str()))
    {
        ImGui::SetNextItemWidth(65.f);
        ImGui::DragInt(("##offsetX" + StringId()).c_str(), &offsetX, 1.f, INT_MIN, INT_MAX, "x: %i");
        ImGui::SetNextItemWidth(65.f);
        ImGui::SameLine();
        ImGui::DragInt(("##offsetY" + StringId()).c_str(), &offsetY, 1.f, INT_MIN, INT_MAX, "y: %i");
        ImGui::SameLine();
        ImGui::Text("Offset");

        ImGui::SetNextItemWidth(65.f);
        ImGui::DragInt(("Width##" + StringId()).c_str(), &width, 1.f, 0,
                       rl::GetScreenWidth());

        ImGui::SameLine();
        ImGui::SetNextItemWidth(65.f);
        ImGui::DragInt(("Height##" + StringId()).c_str(), &height, 1.f, 0,
                       rl::GetScreenHeight());

        ImGui::SetNextItemWidth(65.f);
        ImGui::DragInt(("Border thickness##" + StringId()).c_str(), &borderThickness, 1.f, 0, 200);

        if(ImGui::ColorButton(("Background color##" + StringId()).c_str(), Color::RlToIm(color),
                              ImGuiColorEditFlags_AlphaPreview, ImVec2(20, 20)))
            Debug::DisplayColorPicker(&color);
        ImGui::SameLine();
        if(ImGui::SmallButton(("Background color##btn" + StringId()).c_str()))
            Debug::DisplayColorPicker(&color);

        ImGui::SameLine();
        ImGui::TextDisabled("|");
        ImGui::SameLine();
        if(ImGui::ColorButton(("Border color##" + StringId()).c_str(), Color::RlToIm(borderColor),
                              ImGuiColorEditFlags_AlphaPreview, ImVec2(20, 20)))
            Debug::DisplayColorPicker(&borderColor);
        ImGui::SameLine();
        if(ImGui::SmallButton(("Border color##btn" + StringId()).c_str()))
            Debug::DisplayColorPicker(&borderColor);

        if(ImGui::ColorButton(("Corner color##" + StringId()).c_str(), Color::RlToIm(cornerColor),
                              ImGuiColorEditFlags_AlphaPreview, ImVec2(20, 20)))
            Debug::DisplayColorPicker(&cornerColor);
        ImGui::SameLine();
        if(ImGui::SmallButton(("Corner color##btn" + StringId()).c_str()))
            Debug::DisplayColorPicker(&cornerColor);
    }
}

#endif
