#pragma once

#include "raylib_namespace.h"
#include "CDrawable.h"
#include "ColorExtension.h"

class CTexture : public CDrawable
{
public:
    DEFINE_COMPONENT_DERIVED(CDrawable, CTexture)

    int offsetX = 0, offsetY = 0;
    rl::Rectangle textureRectangle = { 0, 0, 0, 0 };
    rl::Color color = Color::White;

    explicit CTexture(const EntityId& entity);

    void Draw() override;

    [[nodiscard]]
    rl::Vector2 GetSize();

#ifdef DEBUG_TOOLS
    void DebugGui() override;
    void DebugDraw() override;
#endif
};
