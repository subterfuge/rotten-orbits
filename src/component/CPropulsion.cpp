#include "CPropulsion.h"

#include "Debug.h"
#include "component/CTransform.h"
#include "component/CMass.h"

CPropulsion::CPropulsion(const EntityId& entity)
    : Component(entity)
{}

void CPropulsion::StartEngine()
{
    breaking = false;
    stopped = false;
}

#ifdef DEBUG_TOOLS

void CPropulsion::DebugGui()
{
    ImGui::PushID(DEBUG_GUI_ID(CPropulsion));
    ImGui::SetNextItemOpen(true, ImGuiCond_Once);
    if(ImGui::CollapsingHeader("Propulsion"))
    {
        if(ImGui::Button("Start engine"))
        {
            StartEngine();
        }
        ImGui::SetNextItemWidth(65.f);
        ImGui::DragFloat("##x", &target.x, 1.f, -2000.f,
                         2000.f, "x: %.0f");
        ImGui::SameLine();
        ImGui::SetNextItemWidth(65.f);
        ImGui::DragFloat("##y", &target.y, 1.f, -2000.f,
                         2000.f, "y: %.0f");
        ImGui::SameLine();
        ImGui::Text("Target");

        ImGui::SliderFloat("Max speed", &maxSpeed, 0.f, 1000.f);
        ImGui::SliderFloat("Acceleration", &acceleration, 0.f, 1000.f);

        bool previousDisplayCheckbox = displayCheckbox_;
        ImGui::Checkbox("Draw vectors", &displayCheckbox_);
        if(displayCheckbox_ != previousDisplayCheckbox)
        {
            if(displayCheckbox_)
                Debug::AddToDrawList(this);
            else
                Debug::RemoveFromDrawList(this);
        }

        auto* massComponent = ComponentStore::Get<CMass>(GetEntityId());
        float timeToBreak = GetAccelerationTime(massComponent->GetSpeed());
        ImGui::InputFloat("Time to break", &timeToBreak);
    }
    ImGui::PopID();
}

void CPropulsion::DebugDraw()
{
    EntityId en = GetEntityId();
    auto* t = ComponentStore::Get<CTransform>(en);
    if(t == nullptr)
        return;

    rl::DrawLine(t->position.x, t->position.y, t->position.x + targetVector.x, t->position.y + targetVector.y, Color::DebugRed);
    auto* massComponent = ComponentStore::Get<CMass>(GetEntityId());
    rl::DrawLine(t->position.x, t->position.y, t->position.x + massComponent->velocity.x,
                 t->position.y + massComponent->velocity.y, Color::DebugBlue);
    rl::Vector2 velocityEndpoint = rl::Vector2Add(t->position, massComponent->velocity);
    rl::DrawLine(velocityEndpoint.x, velocityEndpoint.y, velocityEndpoint.x + steeringVector.x,
                 velocityEndpoint.y + steeringVector.y, Color::DebugGreen);
    rl::DrawCircleLines(t->position.x, t->position.y, GetBreakDistance(massComponent->GetSpeed()), Color::White);
}

#endif
