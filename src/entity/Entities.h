#pragma once

#include "Alias.h"

namespace Entity
{
    EntityId Player();

    EntityId Window();
    EntityId DialogBox();
}
