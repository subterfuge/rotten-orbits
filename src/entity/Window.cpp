#include "Entities.h"

#include "raylib_namespace.h"
#include "ColorExtension.h"
#include "Maths.h"
#include "Entity.h"
#include "component/CFrame.h"
#include "component/CDrag.h"
#include "component/CDragBack.h"
#include "component/CLoading.h"
#include "component/CText.h"
#include "component/CAnimation.h"
#include "component/CTransform.h"
#include "component/CDialogText.h"

namespace Entity
{
    EntityId Window()
    {
        EntityId id = EntityBuilder::Create("Window");
        auto* transform = ComponentStore::Build<CTransform>(id);

        auto* frame = ComponentStore::Build<CFrame>(id);
        frame->color = Color::Background;
        frame->borderColor = Color::WindowBorder;
        frame->cornerColor = Color::WindowCorner;

        return id;
    }

    EntityId DialogBox()
    {
        EntityId id = EntityBuilder::Create("DialogBox");
        auto* transform = ComponentStore::Build<CTransform>(id);
        int height = 140;
        int width = 750;
        transform->position = rl::Vector2 { static_cast<float>(rl::GetScreenWidth() - width) / 2.f,
                                            static_cast<float>(rl::GetScreenHeight() - height) - 20.f };

        auto* frame = ComponentStore::Build<CFrame>(id);
        frame->height = height;
        frame->width = width;
        frame->color = Color::Background;
        frame->borderColor = Color::WindowBorder;
        frame->cornerColor = Color::WindowCorner;

        auto* frame2 = ComponentStore::Build<CFrame>(id);
        frame2->width = frame2->height = 120;
        frame2->offsetX = frame2->offsetY = 10;
        frame2->color = Color::Transparent;
        frame2->borderColor = Color::WindowBorder;
        frame2->cornerColor = Color::WindowCorner;

        ComponentStore::Build<CDragBack>(id);

        auto* drag = ComponentStore::Build<CDrag>(id);
        drag->dragZone = RectangleI { 0, 0, width, height };
        drag->deadZone = 25.f;
        drag->onDrag([](EntityId id, int x, int y)
        {
            auto* t = ComponentStore::Get<CTransform>(id);
            auto* f = ComponentStore::Get<CFrame>(id);
            auto* dragBack = ComponentStore::Get<CDragBack>(id);
            dragBack->computeXFrame(x);
            dragBack->computeYFrame(y);
            t->position.x += x;
            t->position.y += y;
            rl::Vector2 dragBackFrame = Maths::Clamp(t->position, { 0.f, 0.f },
                                                     { static_cast<float>(rl::GetScreenWidth() - f->width),
                                                       static_cast<float>(rl::GetScreenHeight() - f->height) });
            if(dragBack->x == 0 && dragBackFrame.x != 0)
                dragBack->x = dragBackFrame.x;
            if(dragBack->y == 0 && dragBackFrame.y != 0)
                dragBack->y = dragBackFrame.y;
        });
        drag->onDrop([](EntityId id, int dropPositionX, int dropPositionY) {
            ComponentStore::Get<CDragBack>(id)->reset();
        });

        auto* animation = ComponentStore::Build<CAnimation>(id);
        Keyframe kf = { 0.2f, 1.f, &Easing::Linear };
        animation->addKeyframe(kf);
        animation->onPlay([=](EntityId en, float v) {
            frame->height = rl::Lerp(0.f, 140.f, v);
            frame2->height = rl::Lerp(0.f, 120.f, v);
            frame2->offsetY = rl::Lerp(0.f, 10.f, v);
        });
        animation->play();

        auto* dialogText = ComponentStore::Build<CDialogText>(id);
        dialogText->text = "Oui bonjour";
        dialogText->offset = { 140.f, 20.f };
        dialogText->playing = true;
        dialogText->speed = 30.f;

        return id;
    }
};