#include "Entities.h"

#include "Entity.h"
#include "ResourceStore.h"
#include "component/CMass.h"
#include "component/CTransform.h"
#include "component/CTexture.h"
#include "component/CPropulsion.h"

namespace Entity
{
    EntityId Player()
    {
        const EntityId id = EntityBuilder::Create("Player");
        auto* t = ComponentStore::Build<CTransform>(id);
        auto* texture = ComponentStore::Build<CTexture>(id);
        auto* prop = ComponentStore::Build<CPropulsion>(id);
        auto* massComp = ComponentStore::Build<CMass>(id);
        massComp->mass = 1.f;
        texture->textureRectangle = TextureAtlas::Player;
        rl::Vector2 size = texture->GetSize();
        texture->offsetX = -static_cast<int>(std::round(size.x / 2.f));
        texture->offsetY = -static_cast<int>(std::round(size.y / 2.f));
        t->position = { rl::GetScreenWidth() / 2.f, rl::GetScreenHeight() / 2.f };
        prop->target = t->position;
        return id;
    }
}
