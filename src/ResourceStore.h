#pragma once

#include <vector>
#include "raylib_namespace.h"
#include "Macros.h"

namespace TextureAtlas
{
    constexpr rl::Rectangle Player = { 14, 0, 17, 17 };
    constexpr rl::Rectangle Loading = { 0, 0, 8, 6 };
    constexpr rl::Rectangle TopRightCorner = { 9, 0, 5, 5 };
    constexpr rl::Rectangle TopLeftCorner = { 8, 0, 5, 5 };
    constexpr rl::Rectangle BottomRightCorner = { 9, 1, 5, 5 };
    constexpr rl::Rectangle BottomLeftCorner = { 8, 1, 5, 5 };
    constexpr rl::Rectangle Ship = { 0, 6, 5, 7 };
    constexpr rl::Rectangle ShipOutline = { 23, 17, 9, 13 };
    constexpr rl::Rectangle Warning = { 0, 17, 23, 24 };
    constexpr rl::Rectangle WarningOutline = { 32, 0, 31, 31 };

    rl::Texture GetTexture();
    rl::Font GetFont();
    void UnloadTexture();
}

namespace AudioResource
{
    enum class Sound
    {

    };

    enum class Bleep
    {
        Bleep1,
        Bleep2,
        Bleep3,
        Bleep7,
        Click1,
        Click2,
        Click3,
    };

    rl::Sound GetSound(Sound sound);
    rl::Sound GetBleep(Bleep bleep);
    rl::Sound GetRandomBleep(std::vector<Bleep> bleeps);

    void UnloadSounds();
}
