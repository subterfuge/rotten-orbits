#include "ResourceStore.h"

#include <vector>
#include <stdexcept>
#include "../resources/texture_map.h"

namespace TextureAtlas
{
    namespace
    {
        const rl::Image image_ = { TEXTURE_MAP_DATA, TEXTURE_MAP_WIDTH, TEXTURE_MAP_HEIGHT, 1, TEXTURE_MAP_FORMAT };
    }

    rl::Texture GetTexture()
    {
        static rl::Texture texture = rl::LoadTextureFromImage(image_);
        return texture;
    }

    rl::Font GetFont()
    {
        static rl::Font font = rl::LoadFontEx("resources/ProggyClean.ttf", 13, 0, 0);
        return font;
    }

    void UnloadTexture()
    {
        rl::UnloadTexture(GetTexture());
    }
}

namespace AudioResource
{
    rl::Sound GetBleep(Bleep bleep)
    {
        switch(bleep)
        {
            case Bleep::Bleep1:
                static rl::Sound bleep1 = rl::LoadSound("resources/sound/bleeps/Bleep_01.wav");
                return bleep1;
            case Bleep::Bleep2:
                static rl::Sound bleep2 = rl::LoadSound("resources/sound/bleeps/Bleep_02.wav");
                return bleep2;
            case Bleep::Bleep3:
                static rl::Sound bleep3 = rl::LoadSound("resources/sound/bleeps/Bleep_03.wav");
                return bleep3;
            case Bleep::Bleep7:
                static rl::Sound bleep7 = rl::LoadSound("resources/sound/bleeps/Bleep_07.wav");
                return bleep7;
            case Bleep::Click1:
                static rl::Sound click1 = rl::LoadSound("resources/sound/bleeps/Click_01.wav");
                return click1;
            case Bleep::Click2:
                static rl::Sound click2 = rl::LoadSound("resources/sound/bleeps/Click_02.wav");
                return click2;
            case Bleep::Click3:
                static rl::Sound click3 = rl::LoadSound("resources/sound/bleeps/Click_03.wav");
                return click3;
            default:
                throw std::invalid_argument("Bleep not found");
        }
    }

    rl::Sound GetRandomBleep(std::vector<Bleep> bleeps)
    {
        if(bleeps.empty())
            throw std::invalid_argument("Empty argument 'bleeps' is not allowed.");

        return GetBleep(bleeps[rl::GetRandomValue(0, bleeps.size() - 1)]);
    }
}
