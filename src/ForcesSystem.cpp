#include "ForcesSystem.h"

#include "component/CTransform.h"
#include "component/CPropulsion.h"
#include "component/CMass.h"

namespace ForcesSystem
{
    constexpr float STEERING_THRESHOLD = 0.01f;
    constexpr float DISTANCE_THRESHOLD = 0.8f;
    constexpr float SPEED_THRESHOLD = 0.8f;

    void Update(double dt)
    {
        ComponentStore::ForEach<CMass>([&dt](CMass* massComponent) {
            EntityId entity = massComponent->GetEntityId();
            auto* t = ComponentStore::Get<CTransform>(entity);
            auto* propulsion = ComponentStore::Get<CPropulsion>(entity);
            assert(t != nullptr);
            assert(propulsion != nullptr);

            if(propulsion->stopped)
                return;

            rl::Vector2 v = rl::Vector2Subtract(propulsion->target, t->position);
            float vLength = rl::Vector2Length(v);
            float speed = massComponent->GetSpeed();
            propulsion->targetVector = rl::Vector2Normalize(v);
            propulsion->targetVector = rl::Vector2Scale(propulsion->targetVector, propulsion->maxSpeed);
            // we break if we are close to the target, depending on the distance to break
            // will overshoot if velocity magnitude goes over the break distance
            if(propulsion->GetBreakDistance(speed) >= vLength || propulsion->breaking)
            {
                propulsion->breaking = true;
                propulsion->targetVector = rl::Vector2ClampValue(propulsion->targetVector, 0.f, vLength);
            }
            propulsion->steeringVector = rl::Vector2Normalize(rl::Vector2Subtract(propulsion->targetVector, massComponent->velocity));
            propulsion->steeringVector = rl::Vector2Scale(propulsion->steeringVector, propulsion->acceleration);
            // we steer less if we are close to the target velocity
            if(propulsion->maxSpeed - propulsion->maxSpeed * STEERING_THRESHOLD < speed)
                propulsion->steeringVector = rl::Vector2ClampValue(propulsion->steeringVector, 0.f, rl::Vector2Length(rl::Vector2Subtract(propulsion->targetVector, massComponent->velocity)));
            massComponent->velocity = rl::Vector2ClampValue(
                    rl::Vector2Add(massComponent->velocity, rl::Vector2Scale(propulsion->steeringVector, dt)), 0.f, propulsion->maxSpeed);
            t->position = rl::Vector2Add(t->position, rl::Vector2Scale(massComponent->velocity, dt));
            if(propulsion->breaking && vLength <= DISTANCE_THRESHOLD && speed <= SPEED_THRESHOLD)
            {
                propulsion->stopped = true;
                propulsion->steeringVector = rl::Vector2();
            }
        });
    }
}