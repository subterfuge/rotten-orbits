#include <iostream>
#include <memory>
#define RAYMATH_IMPLEMENTATION
#include "lib/rayfraction/include/raylib_namespace.h"
#undef RAYMATH_IMPLEMENTATION
#include "Game.h"
#include "scene/MainScene.h"

int main() {
    constexpr int screenWidth = 1200;
    constexpr int screenHeight = 750;
    std::unique_ptr<Game> game = std::make_unique<Game>(screenWidth, screenHeight, "Rotten Orbits");
    Scene* scene = new MainScene();
    game->SwitchScene(scene);
    //rl::SetTargetFPS(60);
    game->Exec();
    return 0;
}
